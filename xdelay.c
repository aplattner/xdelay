#include <xorg-server.h>
#include <xf86Module.h>
#include <xf86.h>
#include <os.h>
#include <extension.h>
#include <unistd.h>

static int delay = 4;

static void xdelayExtensionInit(void)
{
    xf86Msg(X_INFO, "Delaying startup by %i seconds\n", delay);
    sleep(delay);
}

static ExtensionModule xdelayExtensionModule = {
    .initFunc = xdelayExtensionInit,
    .name = "XDelay",
};

static void* xdelaySetup(void *module, void *options, int *errmaj, int *errmin)
{
    LoadExtensionList(&xdelayExtensionModule, 1, FALSE);
    return (void*)1;
}

static XF86ModuleVersionInfo xdelayModuleVersionInfo = {
    "xdelay",
    "Aaron Plattner",
    MODINFOSTRING1,
    MODINFOSTRING2,
    XORG_VERSION_CURRENT,
    0, 0, 0,
    ABI_CLASS_EXTENSION,
    ABI_EXTENSION_VERSION,
    MOD_CLASS_EXTENSION,
    { 0, 0, 0, 0 }
};

_X_EXPORT
XF86ModuleData xdelayModuleData = {
    &xdelayModuleVersionInfo,
    xdelaySetup,
    0
};
